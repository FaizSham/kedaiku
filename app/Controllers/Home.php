<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {

        $all_pekan = [];

        $pekan = [
            'nama' => 'Seremban',
            'gambar' => 'https://images.unsplash.com/photo-1570036340722-b301922be4b5?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTV8fG5lZ2VyaSUyMHNlbWJpbGFufGVufDB8fDB8fHww&auto=format&fit=crop&w=1000&q=60',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, praesentium. Repellendus, quidem sint ex voluptatem temporibus natus? Cum quod ea debitis, et facere animi numquam vitae, quisquam, doloribus ipsa suscipit.'
            
        ];

        $all_pekan[] = $pekan;

        $pekan = [
            'nama' => 'Gemas',
            'gambar' => 'https://images.unsplash.com/photo-1612696964842-af32a6788bd1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8bmVnZXJpJTIwc2VtYmlsYW58ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=1000&q=60',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, praesentium. Repellendus, quidem sint ex voluptatem temporibus natus? Cum quod ea debitis, et facere animi numquam vitae, quisquam, doloribus ipsa suscipit.'
            
        ];

        $all_pekan[] = $pekan;

        $pekan = [
            'nama' => 'Port Dickson',
            'gambar' => 'https://images.unsplash.com/photo-1583159075864-4a3beb6fd55d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8bmVnZXJpJTIwc2VtYmlsYW58ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=1000&q=60',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, praesentium. Repellendus, quidem sint ex voluptatem temporibus natus? Cum quod ea debitis, et facere animi numquam vitae, quisquam, doloribus ipsa suscipit.'
            
        ];

        $all_pekan[] = $pekan;

        return view('homepage', [ 'all_pekan' => $all_pekan ]);
    }   

}
